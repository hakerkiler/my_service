<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TypeProject extends Model
{
    protected $table = 'type_project';
}
