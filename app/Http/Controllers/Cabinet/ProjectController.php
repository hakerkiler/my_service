<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Model\Users;
use App\Model\TypeProject;
use App\Model\Project;
use Illuminate\Http\Request;
use App\Http\Requests;

Class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Project::latest('created_at')->get();

        return view('cabinet.project.index',['projects' => $project]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // TODO: de facut lista de pagini ale utilizatorului autentificat
        $select = array(
            '1' => 'dsads'
        );

        $typeProject = TypeProject::select('id', 'name')->get();
        //dd($typeProject->all());
        foreach($typeProject->all() as $one){
            $typeProject_arr[$one['id']] = $one['name'];
        }
        return view('cabinet.project.create', ['selectname' => $select, 'typeProjects' => $typeProject_arr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type_id' => 'required',
            'project_name' => 'required',
            'description' => 'required'
        ]);

        $projectModel = new Project();
        $projectModel->type_id = $request->input('type_id');
        $projectModel->user_id = 1; //TODO: de pus autentificarea si de luat id la utilizator
        $projectModel->project_name = $request->input('project_name');
        $projectModel->land_page = $request->input('land_page');
        $projectModel->ref_link = $request->input('ref_link');
        $projectModel->description = $request->input('description');
        $projectModel->active = 0;
        $projectModel->save();
        return redirect()->route('cabinet.project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);

        return view('cabinet.project.show')->with('project', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // TODO: de facut lista de pagini ale utilizatorului autentificat
        $select = array(
            '1' => 'dsads'
        );

        $typeProject = TypeProject::select('id', 'name')->get();
        $typeProject_arr = array();
        foreach($typeProject->all() as $one){
            $typeProject_arr[$one['id']] = $one['name'];
        }

        $project = Project::find($id);

        return view('cabinet.project.edit')
            ->withProject($project)
            ->with('selectname', $select)
            ->with('typeProjects', $typeProject_arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $this->validate($request, [
            'type_id' => 'required',
            'project_name' => 'required',
            'description' => 'required'
        ]);

        // process the login  route('cabinet.project.edit', $project->id )

        // store
        $projectModel = Project::find($id);
        $projectModel->type_id = $request->input('type_id');
        $projectModel->user_id = 1; //TODO: de pus autentificarea si de luat id la utilizator
        $projectModel->project_name = $request->input('project_name');
        $projectModel->land_page = $request->input('land_page');
        $projectModel->ref_link = $request->input('ref_link');
        $projectModel->description = $request->input('description');
        $projectModel->active = $request->input('active');
        $projectModel->save();

        // redirect
        //Session::flash('message', 'Successfully updated project!');
        return redirect()->route('cabinet.project.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $projectModel = Project::find($id);
        $projectModel->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the Project!');
        return Redirect::to('cabinet.project.index');
    }
}
