<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => '/cabinet'], function(){
    Route::resource('project', 'Cabinet\ProjectController');
    Route::resource('page', 'Cabinet\ProjectController');

    Route::get('/', [
        'uses' => 'Cabinet\HomeController@index',
        'as'   => 'cabinet.home'
    ]);

    Route::get('/', [
        'uses' => 'Cabinet\HomeController@index',
        'as'   => 'cabinet.home'
    ]);

});




Route::get('/cabinet/info', function(){
        return redirect()->route('cabinet.home')->with('info', 'info_test');
});