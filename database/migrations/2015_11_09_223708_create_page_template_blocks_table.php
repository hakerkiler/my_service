<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTemplateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_templates_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id');
            $table->integer('type_id');
            $table->string('name');
            $table->string('description');
            $table->text('json_settings');
            $table->integer('position');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_templates_blocks');
    }
}
