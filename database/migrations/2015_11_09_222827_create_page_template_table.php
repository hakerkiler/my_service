<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('description');
            $table->string('screenshot');
            $table->integer('number_blocks');
            $table->integer('use_number')->default(0);
            $table->boolean('active');
            $table->string('preview_link');
            $table->timestamps();
        });

        /*Schema::table('page_templates')->insert(
            array(
                'user_id'       => 1,
                'name'          => 'Test Template 1',
                'description'   => 'Description test template 1',
                'number_blocks' => 3,
                'active'        => 1,
                'json_settings' => '{}',
            ),
            array(
                'name' => 'Footer',
            ),
            array(
                'name' => 'Portfolio',
            ),
            array(
                'name' => 'About',
            )
        );*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_templates');
    }
}
