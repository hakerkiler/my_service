<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('type_project')->insert(['name' => 'One page site']);
        DB::table('type_project')->insert(['name' => 'Sale Product']);
        DB::table('type_project')->insert(['name' => 'Project Information']);

        DB::table('page_templates_blocks_type')->insert(['name' => 'Header']);
        DB::table('page_templates_blocks_type')->insert(['name' => 'Footer']);
        DB::table('page_templates_blocks_type')->insert(['name' => 'Items Blocks']);

        /*Model::unguard();

        // $this->call('UserTableSeeder');

        Model::reguard();*/
    }
}
