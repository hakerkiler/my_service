<?php

return [

    'dashboard'             => 'Dashboard',
    'next'                  => 'Next &raquo;',
    'project'               => 'Project',
    'add_project'           => 'Add Project',
    'my_projects'           => 'My Projects',
    'like_projects'         => 'Like Projects',
    'page_title'            => 'Page Generate',
    'add_page'              => 'Add Page',
    'my_page'               => 'My Page',
    'page_template'         => 'Page Template',
    'service_title'         => 'Service',
    'profile'               => 'Profile',
    'pakage'                => 'Pakage',
    'referals'              => 'Referals',
    'statistics'            => 'Statistics',
    'billing'               => 'Billing',
    'my_account'            => 'My Account',


];
