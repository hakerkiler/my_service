<?php

return [

    'project'               => 'Project',
    'add_project'           => 'Add Project',
    'my_projects'           => 'My Projects',
    'like_projects'         => 'Like Projects',

];
