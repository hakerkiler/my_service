<?php

return [
    'create'                => 'Create',
    'update'                => 'Update',
    'type'                  => 'Type',
    'my_projects'           => 'My Projects',
    'like_projects'         => 'Like Projects',
    'active_project'        => 'Active Project for promovation',
    'project_name'          => 'Project Name',
    'description'           => 'Description',
    'ref_link'              => 'Ref Link (IF have a page for this project)',
    'land_page'             => 'Land Page',

];
