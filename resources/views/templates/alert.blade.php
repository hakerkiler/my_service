
@if( Session::has('info'))
    new TabbedNotification({
        title: '{{ trans('notification_info') }}',
        text: '{{ trans(Session::get('info')) }}',
        type: 'success',
        sound: false
    })
@endif

@if( Session::has('error'))
    new TabbedNotification({
        title: '{{ trans('notification_error') }}',
        text: '{{ trans(Session::get('error')) }}',
        type: 'error',
        sound: false
    })
@endif

@if( Session::has('warning'))
    new TabbedNotification({
        title: '{{ trans('notification_warning') }}',
        text: '{{ trans(Session::get('warning')) }}',
        type: 'warning',
        sound: false
    })
@endif