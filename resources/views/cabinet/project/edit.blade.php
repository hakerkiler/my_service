@extends('templates.app')

@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Projects </h3>
            </div>
            <div class="title_right text-right">
                <a href="{{ route('cabinet.project.index') }}" class="btn btn-success btn-sm">List Project</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_content">

                        {!! Form::model($project,[
                            'method' => 'PATCH',
                            'route' => ['cabinet.project.update', $project->id]
                            ]) !!}

                        @include('cabinet.project.form')

                        <div class="form-group">
                            {!!  Form::submit( trans('form.update'), ['class'=>'btn btn-primary'] )  !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop