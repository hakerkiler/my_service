@extends('templates.app')

@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Projects </h3>
            </div>
            <div class="title_right text-right">
                <a href="{{ route('cabinet.project.create') }}" class="btn btn-success btn-sm">Add Project</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <!-- start project list -->
                        <table class="table table-striped projects">
                            <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">Project Name</th>
                                <th>Project Description</th>
                                <th>Project Progress</th>
                                <th>Status</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                            </thead>
                            @if( count($projects) > 0 )
                            <tbody>
                            @foreach( $projects as $project )
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>{{ $project->project_name }}</a>
                                        <br>
                                        <small>Created {{ $project->created_at }}</small>
                                    </td>
                                    <td>
                                        {{ $project->description }}
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="57" aria-valuenow="56" style="width: 57%;"></div>
                                        </div>
                                        <small>57% Complete</small>
                                    </td>
                                    <td>
                                        @if( $project->active )
                                            <button type="button" class="btn btn-success btn-xs">Active</button>
                                        @else
                                            <button type="button" class="btn btn-danger btn-xs">Inactive</button>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('cabinet.project.show', $project->id ) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="{{ route('cabinet.project.edit', $project->id ) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>

                                            {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['cabinet.project.destroy', $project->id]
                                                ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                            {!! Form::close() !!}

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                        </table>
                        <!-- end project list -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop