@extends('templates.app')

@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Projects </h3>
            </div>
            <div class="title_right text-right">
                <a href="{{ route('cabinet.project.index') }}" class="btn btn-success btn-sm">List Project</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_content">

                        {!! Form::open(['route'=>'cabinet.project.store']) !!}

                        <div class="form-group">
                            {!!  Form::label('type')  !!}
                            {!!  Form::select('type_id', $typeProjects, Input::old('type_id'), array('class'=>'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!!  Form::label('project_name')  !!}
                            {!!  Form::text( 'project_name', Input::old('project_name'), ['class'=>'form-control'] )  !!}
                        </div>

                        <div class="form-group">
                            {!!  Form::label('description')  !!}
                            {!!  Form::textarea( 'description', Input::old('description'), ['class'=>'form-control'] )  !!}
                        </div>

                        <div class="form-group">
                            {!!  Form::label('ref_link')  !!}
                            {!!  Form::text( 'ref_link', Input::old('ref_link'), ['class'=>'form-control'] )  !!}
                        </div>

                        <div class="form-group">
                            {!!  Form::label('land_page', 'Page Generated')  !!}
                            {!!  Form::select('land_page', $selectname, Input::old('land_page'), array('class'=>'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!!  Form::submit( 'Create', ['class'=>'btn btn-primary'] )  !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop