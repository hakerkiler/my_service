
{{--// forma de introducere in baza a proiectelor--}}

<div class="form-group">
    {!!  Form::label('type_id', trans('form.type'))  !!}
    {!!  Form::select('type_id', $typeProjects, Input::old('type_id'), array('class'=>'form-control')) !!}
</div>

<div class="form-group">
    {!!  Form::label('project_name', trans('form.project_name'))  !!}
    {!!  Form::text( 'project_name', Input::old('project_name'), ['class'=>'form-control'] )  !!}
</div>

<div class="form-group">
    {!!  Form::label('description', trans('form.description'))  !!}
    {!!  Form::textarea( 'description', Input::old('description'), ['class'=>'form-control'] )  !!}
</div>

<div class="form-group">
    {!!  Form::label('ref_link', trans('form.ref_link'))  !!}
    {!!  Form::text( 'ref_link', Input::old('ref_link'), ['class'=>'form-control'] )  !!}
</div>

<div class="form-group">
    {!!  Form::label('land_page', trans('form.land_page'))  !!}
    {!!  Form::select('land_page', $selectname, Input::old('land_page'), array('class'=>'form-control')) !!}
</div>

<div class="form-group">
    {!!  Form::label('active', trans('form.active_project'))  !!}
    {!!  Form::checkbox('active', 1, Input::old('active')) !!}
</div>