@extends('templates.app')

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>{{ trans('meniu.dashboard') }} </h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('meniu.my_account') }} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="bs-docs-section">
                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.profile') }} </span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.pakage') }} </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.referals') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.statistics') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.billing') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('meniu.project') }} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="bs-docs-section">
                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">
                                <li>
                                    <a href="{{ route('cabinet.project.create') }}">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.add_project') }} </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ route('cabinet.project.index') }}">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.my_projects') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.like_projects') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('meniu.page_title') }} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="bs-docs-section">
                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.add_page') }} </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.my_page') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.page_template') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('meniu.service_title') }} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="bs-docs-section">
                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.tiser_promotion') }} </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.skype_promotion') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.yandex_direct') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.google_adwords') }}</span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="">
                                        <span class="glyphicon glyphicon-signal" aria-hidden="true"></span>
                                        <span class="glyphicon-class">{{ trans('meniu.statistics') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@stop